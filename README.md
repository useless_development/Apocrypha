# Apocrypha- online player

## Page model
![alt tag](https://gitlab.com/useless_development/Apocrypha/raw/master/examples/page.png)

## Playlists model
![alt tag](https://gitlab.com/useless_development/Apocrypha/raw/master/examples/Playlists.png)

## Landing model
![alt tag](https://gitlab.com/useless_development/Apocrypha/raw/master/examples/Landing.png)

## Artist model
![alt tag](https://gitlab.com/useless_development/Apocrypha/raw/master/examples/Artist.png)
