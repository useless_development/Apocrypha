var ActiveTabId=0;
var Tabs=2;
function getAverage(){
    var Avrg=0; 
    $(".studentsActive .GPA").each(function(){
    Avrg+=parseFloat($(this).html());
}); 
    $(".avGPA").text(Avrg/$(".studentsActive .GPA").length);
        if (!$(".studentsActive .GPA").length)
            {
                 $(".avGPA").text("0");
            }
};

function validate(Name,GPA){
    if (!Name || /^\s*$/.test(Name)){
        $(".resultInfo").text("Error: Student name must not be empty");
        return false;
    };
    if (!GPA || /^\s*$/.test(GPA)){
        $(".resultInfo").text("Error: GPA must not be empty");
        return false;
    };
    if (isNaN(GPA)){
       $(".resultInfo").text("Error: GPA must be a number");
        return false; 
    };
    return true;
};
function addStudent(Name,GPA){
    if(validate(Name,GPA)){
   return "<tr><td>"+Name+"</td><td class='GPA'>"+GPA+"</td><td><button>X</button></td></tr>" ;
    };
};


$(document).ready(function() {
  getAverage();    
$(".submit").click(function(){
    $(".resultInfo").text("Added new student");
     $(".studentsActive .studentsTable").append(addStudent($(".newName").val(),$(".newGPA").val()));
    getAverage();
});

$(".studentsActive .studentsTable").on("click","button",function(){
    $(this).parent().parent().remove();
    getAverage();
});
    
$(".up-nav").on("click",".tab",function(){
    var ActiveTmp=$(".tab").index($(this));
    if(ActiveTmp==ActiveTabId){
        return;
    }
    $(".students:eq("+ActiveTabId+")").toggleClass("studentsActive");
    $(".tab:eq("+ActiveTabId+")").toggleClass("tabActive");
    ActiveTabId=ActiveTmp;
    $(".students:eq("+ActiveTabId+")").toggleClass("studentsActive");
    $(".tab:eq("+ActiveTabId+")").toggleClass("tabActive");
     getAverage();
}); 
    
$(".addtab").click(function(){
    Tabs++;
    $(".tab").last().after('<div class="tab"><p>Tab '+Tabs+'</p></div>');
   $(".students").last().after('<div class="students"><table class="studentsTable"><th>Name</th><th>GPA</th><th>Averege GPA: <span class="avGPA">0</span></th></table></div>');
});      
});